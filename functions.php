<?php


$content_width = 640;

/* Google fonti lisamine */

function add_google_fonts() {
  wp_enqueue_style( 'google_web_fonts', 'https://fonts.googleapis.com/css?family=Merriweather+Sans' );
}

add_action( 'wp_enqueue_scripts', 'add_google_fonts' );

/*
	==========================================
	Scriptide lisamine
	==========================================
*/

function kirik_scripts() {
	wp_enqueue_style( 'kirik-style', get_stylesheet_uri() );

	wp_enqueue_script( 'kirik-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0.0', true );

    wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/kirik.css', array(), '1.0.0', 'all');

	wp_enqueue_script( 'kirik-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '1.0.0', true );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'kirik_scripts' );


/*
	==========================================
	 Menyy aktiveerimine
	==========================================
*/

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Menu', 'kirik' ),
		) );


if ( ! function_exists( 'kirik_setup' ) ) :

	function kirik_setup() {

		load_theme_textdomain( 'kirik', get_template_directory() . '/languages' );
		
		
/*
	==========================================
	 Theme support function
	==========================================
*/

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'kirik_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'kirik_setup' );

/*
	==========================================
	Set the content width
	==========================================
*/

function kirik_content_width() {

	$GLOBALS['content_width'] = apply_filters( 'kirik_content_width', 640 );
}
add_action( 'after_setup_theme', 'kirik_content_width', 0 );

/*
	==========================================
    Register widget area
	==========================================
function kirik_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'kirik' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'kirik' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'kirik_widgets_init' );
*/ 


/*
	==========================================
	Implement the Custom Header feature.
	==========================================
*/

require get_template_directory() . '/inc/custom-header.php';

require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/template-functions.php';

require get_template_directory() . '/inc/customizer.php';

if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
